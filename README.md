#Movies

##App architecture
- **MVP**

##Dependencies
- Retrofit2
- RxJava2
- Dagger2
- Glide
- Mockito2

##Overview
- Regarding the filtering feature, the data is filtered locally as otherwise wasn't specified in the requirements.
And since I showed how to make a request, I preferred to do the local filter.

- When using a dependency injection framework like **Dagger**, I prefer to create a component for each fragment, this way we ensure that
fragments are decoupled from the host activity.
