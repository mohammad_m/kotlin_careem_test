package com.app.movies.usecase

import com.app.movies.model.ResponseConfiguration
import com.app.movies.network.IServiceProvider
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable

class ConfigurationUseCase(private val serviceProvider: IServiceProvider,
                           private val ioThread: Scheduler,
                           private val mainThread: Scheduler) {

    private var callback: Callback? = null

    interface Callback {
        fun onConfigurationFetched(response: ResponseConfiguration?)
        fun onConfigurationFailed(throwable: Throwable)
    }

    fun execute(callback: Callback): Disposable {
        this.callback = callback
        return serviceProvider.getConfiguration()
                .subscribeOn(ioThread)
                .observeOn(mainThread)
                .subscribe(this::onConfigurationFetched, this::onConfigurationFailed)
    }

    private fun onConfigurationFetched(response: ResponseConfiguration) {
        callback?.onConfigurationFetched(response)
    }

    private fun onConfigurationFailed(throwable: Throwable) {
        callback?.onConfigurationFailed(throwable)
    }
}