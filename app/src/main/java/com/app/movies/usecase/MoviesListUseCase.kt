package com.app.movies.usecase

import com.app.movies.model.ResponseMoviesList
import com.app.movies.network.IServiceProvider
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable

class MoviesListUseCase(private val serviceProvider: IServiceProvider,
                        private val ioThread: Scheduler,
                        private val mainThread: Scheduler) {

    private var callback: Callback? = null

    interface Callback {
        fun onMoviesFetched(response: ResponseMoviesList)
        fun onMoviesFailed(throwable: Throwable)
    }

    fun execute(callback: Callback?, page: Int): Disposable {
        this.callback = callback
        return serviceProvider.getMoviesList(page)
                .subscribeOn(ioThread)
                .observeOn(mainThread)
                .subscribe(this::onMoviesFetched, this::onMoviesFailed)
    }

    private fun onMoviesFetched(response: ResponseMoviesList) {
        callback?.onMoviesFetched(response)
    }

    private fun onMoviesFailed(throwable: Throwable) {
        callback?.onMoviesFailed(throwable)
    }
}