package com.app.movies.utils

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.DatePicker
import java.io.Serializable
import java.util.*

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private var datePickerCallback: DatePickerCallback? = null
    private var dateType: DateType? = null

    interface DatePickerCallback: Serializable {
        fun onDateSet(dateType: DateType?, year: Int, month: Int, day: Int)
    }

    companion object {

        private const val KEY_DATE_TYPE = "KEY_DATE_TYPE"
        private const val KEY_CALLBACK = "KEY_CALLBACK"

        fun newInstance(dateType: DateType, onDateSet: DatePickerCallback): DatePickerFragment {
            val args = Bundle()
            args.putSerializable(KEY_DATE_TYPE, dateType)
            args.putSerializable(KEY_CALLBACK, onDateSet)
            val fragment = DatePickerFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        datePickerCallback = arguments?.getSerializable(KEY_CALLBACK) as DatePickerCallback?
        dateType = arguments?.getSerializable(KEY_DATE_TYPE) as DateType?

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(context, this, year, month, day)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        datePickerCallback?.onDateSet(dateType, year, month, day)
    }
}