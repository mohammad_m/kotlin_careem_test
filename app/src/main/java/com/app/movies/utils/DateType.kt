package com.app.movies.utils

enum class DateType {
    START_DATE, END_DATE
}