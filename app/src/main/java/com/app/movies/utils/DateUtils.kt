package com.app.movies.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun getStringDateFormatted(year: Int, month: Int, day: Int): String {

        val dateFormat = "yyyy-MM-dd"
        val simpleDateFormat = SimpleDateFormat(dateFormat)

        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, day)
        val dateRepresentation = cal.time

        return simpleDateFormat.format(dateRepresentation)
    }
}