package com.app.movies.network

import com.app.movies.model.ResponseConfiguration
import com.app.movies.model.ResponseMoviesList
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("movie/now_playing")
    fun getMoviesList(
            @Query("api_key") apiKey: String,
            @Query("page") page: Int): Observable<ResponseMoviesList>

    @GET("configuration")
    fun getConfiguration(@Query("api_key") apiKey: String): Observable<ResponseConfiguration>
}