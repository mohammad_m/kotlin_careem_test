package com.app.movies.network

import com.app.movies.BuildConfig
import com.app.movies.model.ResponseConfiguration
import com.app.movies.model.ResponseMoviesList
import io.reactivex.Observable

object ServiceProvider : IServiceProvider {

    private const val API_KEY = BuildConfig.TMDB_API_KEY

    override fun getMoviesList(page: Int): Observable<ResponseMoviesList> {
        return ServiceFactory.apiService.getMoviesList(API_KEY, page)
    }

    override fun getConfiguration(): Observable<ResponseConfiguration> {
        return ServiceFactory.apiService.getConfiguration(API_KEY)
    }
}