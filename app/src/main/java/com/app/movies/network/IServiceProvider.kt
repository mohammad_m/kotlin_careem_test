package com.app.movies.network

import com.app.movies.model.ResponseConfiguration
import com.app.movies.model.ResponseMoviesList
import io.reactivex.Observable

interface IServiceProvider {

    fun getMoviesList(page: Int): Observable<ResponseMoviesList>
    fun getConfiguration(): Observable<ResponseConfiguration>
}