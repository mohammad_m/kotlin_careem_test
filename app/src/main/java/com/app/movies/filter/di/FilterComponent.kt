package com.app.movies.filter.di

import com.app.movies.core.di.PerFragment
import com.app.movies.filter.FilterFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [FilterModule::class])
interface FilterComponent {
    fun inject(fragment: FilterFragment)
}