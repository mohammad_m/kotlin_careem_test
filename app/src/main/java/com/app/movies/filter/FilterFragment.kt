package com.app.movies.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.app.movies.R
import com.app.movies.core.BaseFragment
import com.app.movies.core.di.ParentComponent
import com.app.movies.filter.di.FilterModule
import com.app.movies.utils.DatePickerFragment
import com.app.movies.utils.DateType
import com.app.movies.utils.DateUtils
import kotlinx.android.synthetic.main.fragment_filter.view.*
import javax.inject.Inject

class FilterFragment : BaseFragment(), FilterContract.View, DatePickerFragment.DatePickerCallback {

    @Inject
    lateinit var presenter: FilterContract.Presenter

    companion object {
        fun newInstance(): FilterFragment {
            return FilterFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInjector()

        presenter.attachView(this)
    }

    private fun initInjector() {
        (activity as ParentComponent<FilterContract.FilterComp>).getComponent()
                .filterComponent(FilterModule(activity as FilterContract.Navigator)).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showDatePicker(DateType.START_DATE)

        view.apply {
            startDateLabel.setOnClickListener { presenter.onStartDateClicked() }
            endDateLabel.setOnClickListener { presenter.onEndDateClicked() }
            filterApplyButton.setOnClickListener { presenter.onApplyButtonClicked(startDateTv.text, endDateTv.text) }
        }
    }

    override fun showDatePicker(dateType: DateType) {
        DatePickerFragment.newInstance(dateType, this)
                .show(activity?.supportFragmentManager, DatePickerFragment::class.qualifiedName)
    }

    override fun onDateSet(dateType: DateType?, year: Int, month: Int, day: Int) {
        if (DateType.START_DATE == dateType) {
            view?.startDateTv?.text = DateUtils.getStringDateFormatted(year, month, day)
        } else {
            view?.endDateTv?.text = DateUtils.getStringDateFormatted(year, month, day)
        }
    }

    override fun showErrorMessage() {
        Toast.makeText(context, R.string.filter_error, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deAttachView()
    }
}