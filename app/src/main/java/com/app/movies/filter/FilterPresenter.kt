package com.app.movies.filter

import android.text.TextUtils
import com.app.movies.core.BasePresenter
import com.app.movies.utils.DateType

class FilterPresenter(val navigator: FilterContract.Navigator) : BasePresenter<FilterContract.View>(), FilterContract.Presenter {

    override fun onStartDateClicked() {
        getView()?.showDatePicker(DateType.START_DATE)
    }

    override fun onEndDateClicked() {
        getView()?.showDatePicker(DateType.END_DATE)
    }

    override fun onApplyButtonClicked(startDate: CharSequence, endDate: CharSequence) {
        if (!startDate.isNullOrEmpty() && !endDate.isNullOrEmpty()) {
            navigator.onApplyFilterClicked(startDate.toString(), endDate.toString())
        }
        else {
            getView()?.showErrorMessage()
        }
    }
}