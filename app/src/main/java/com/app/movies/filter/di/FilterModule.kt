package com.app.movies.filter.di

import com.app.movies.core.di.PerFragment
import com.app.movies.filter.FilterContract
import com.app.movies.filter.FilterPresenter
import dagger.Module
import dagger.Provides

@Module
class FilterModule(val navigator: FilterContract.Navigator) {

    @Provides
    @PerFragment
    fun providePresenter(): FilterContract.Presenter {
        return FilterPresenter(navigator)
    }
}