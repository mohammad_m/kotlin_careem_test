package com.app.movies.filter

import com.app.movies.core.MvpPresenter
import com.app.movies.core.MvpView
import com.app.movies.filter.di.FilterComponent
import com.app.movies.filter.di.FilterModule
import com.app.movies.utils.DateType

interface FilterContract {

    interface View : MvpView {
        fun showDatePicker(enD_DATE: DateType)
        fun showErrorMessage()
    }

    interface Presenter : MvpPresenter<View> {
        fun onStartDateClicked()
        fun onEndDateClicked()
        fun onApplyButtonClicked(startDate: CharSequence, endDate: CharSequence)
    }

    interface Navigator {
        fun onApplyFilterClicked(startDate: String?, endDate: String?)
    }

    interface FilterComp {
        fun filterComponent(module: FilterModule): FilterComponent
    }
}