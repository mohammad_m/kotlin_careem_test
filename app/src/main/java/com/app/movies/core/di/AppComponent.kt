package com.app.movies.core.di

import com.app.movies.core.MApplication
import com.app.movies.mainActivity.di.MainActivityComponent
import com.app.movies.mainActivity.di.MainActivityModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: MApplication)

    fun mainActivityComponent(module: MainActivityModule): MainActivityComponent
}