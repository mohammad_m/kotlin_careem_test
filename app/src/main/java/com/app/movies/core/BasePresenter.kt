package com.app.movies.core

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    private var view: V? = null

    private val compositeDisposable = CompositeDisposable()

    override fun attachView(view: V) {
        this.view = view
    }

    fun addDisposable(disposable: Disposable?) {
        if (disposable != null) {
            compositeDisposable.add(disposable)
        }
    }

    override fun deAttachView() {
        compositeDisposable.clear()
        view = null
    }

    fun getView(): V? {
        return view
    }
}