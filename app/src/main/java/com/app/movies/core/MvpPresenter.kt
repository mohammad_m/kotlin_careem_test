package com.app.movies.core

interface MvpPresenter<in V : MvpView> {

    fun attachView(v: V)
    fun deAttachView()
}