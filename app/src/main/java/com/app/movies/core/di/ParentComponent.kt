package com.app.movies.core.di

interface ParentComponent<out C> {

    fun getComponent(): C
}