package com.app.movies.core

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

open abstract class BaseActivity : AppCompatActivity() {

    private val fragmentContainerId: Int by lazy {
        getFragmentContainer()
    }

    abstract fun getFragmentContainer(): Int

    protected fun addFragment(fragment: Fragment, addToBackStack: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
                .add(fragmentContainerId, fragment)
        if (addToBackStack) {
            transaction.addToBackStack(fragment::class.simpleName)
        }
        transaction.commit()
    }

    protected fun replaceFragment(fragment: Fragment, addToBackStack: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
                .replace(fragmentContainerId, fragment)
        if (addToBackStack) {
            transaction.addToBackStack(fragment::class.simpleName)
        }
        transaction.commit()
    }
}