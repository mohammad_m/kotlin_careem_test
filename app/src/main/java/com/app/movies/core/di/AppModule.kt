package com.app.movies.core.di

import com.app.movies.core.MApplication
import com.app.movies.network.IServiceProvider
import com.app.movies.network.ServiceProvider
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule(val app: MApplication) {

    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    @Named("ioThread")
    fun provideIoThread(): Scheduler {
        return Schedulers.newThread()
    }

    @Provides
    @Singleton
    @Named("mainThread")
    fun provideMainThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Singleton
    fun provideServiceProvider(): IServiceProvider {
        return ServiceProvider
    }
}