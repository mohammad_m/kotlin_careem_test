package com.app.movies.core

import android.app.Application
import com.app.movies.core.di.AppComponent
import com.app.movies.core.di.AppModule
import com.app.movies.core.di.DaggerAppComponent

class MApplication : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}