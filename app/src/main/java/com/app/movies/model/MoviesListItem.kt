package com.app.movies.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoviesListItem(val id: String?,
                          val title: String?,
                          val releaseDate: String?,
                          val overview: String?,
                          val posterPath: String?): Parcelable