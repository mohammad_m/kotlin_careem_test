package com.app.movies.model

class MoviesListMapper {

    fun mapResponseListToViewList(response: ResponseMoviesList): ArrayList<MoviesListItem> {
        val responseList = response.results
        val targetList = ArrayList<MoviesListItem>()
        if (responseList != null) {
            for (item: ResponseMoviesList.ResponseMovieListItem in responseList) {
                targetList.add(MoviesListItem(item.id, item.title, item.releaseDate, item.overview, item.posterPath))
            }
        }
        return targetList
    }
}