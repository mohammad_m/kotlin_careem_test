package com.app.movies.model

import com.google.gson.annotations.SerializedName

data class ResponseConfiguration(@SerializedName("images") val imagesConfiguration: ImagesConfiguration) {

    data class ImagesConfiguration(@SerializedName("base_url") val baseUrl: String,
                                   @SerializedName("poster_sizes") val posterSizes: List<String>)

}
