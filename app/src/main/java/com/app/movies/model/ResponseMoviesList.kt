package com.app.movies.model

import com.google.gson.annotations.SerializedName

data class ResponseMoviesList(@SerializedName("results") val results: List<ResponseMovieListItem>?) {

    data class ResponseMovieListItem(@SerializedName("id") val id: String?,
                                     @SerializedName("title") val title: String?,
                                     @SerializedName("release_date") val releaseDate: String?,
                                     @SerializedName("overview") val overview: String?,
                                     @SerializedName("poster_path") val posterPath: String?)
}