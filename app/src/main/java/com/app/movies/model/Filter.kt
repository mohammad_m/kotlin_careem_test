package com.app.movies.model

data class Filter(var startDate: String?,
                  var endDate: String?,
                  var filterApplied: Boolean?)