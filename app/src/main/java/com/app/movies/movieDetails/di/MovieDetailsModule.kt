package com.app.movies.movieDetails.di

import com.app.movies.core.di.PerFragment
import com.app.movies.movieDetails.MovieDetailsContract
import com.app.movies.movieDetails.MovieDetailsPresenter
import dagger.Module
import dagger.Provides

@Module
class MovieDetailsModule {

    @Provides
    @PerFragment
    fun providePresenter(): MovieDetailsContract.Presenter {
        return MovieDetailsPresenter()
    }
}