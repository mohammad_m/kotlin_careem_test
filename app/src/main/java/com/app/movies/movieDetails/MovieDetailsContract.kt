package com.app.movies.movieDetails

import com.app.movies.core.MvpPresenter
import com.app.movies.core.MvpView
import com.app.movies.model.MoviesListItem
import com.app.movies.movieDetails.di.MovieDetailsComponent

interface MovieDetailsContract {

    companion object {
        const val KEY_MOVIE_DETAILS = "KEY_MOVIE_DETAILS"
        const val KEY_BASE_URL: String = "KEY_BASE_URL"
        const val KEY_POSTER_SIZE: String = "KEY_POSTER_SIZE"
    }

    interface View: MvpView {
        fun displayPoster(url: String)
        fun bindTitle(title: String?)
        fun bindReleaseDate(releaseDate: String?)
        fun bindOverview(overview: String?)
    }

    interface Presenter: MvpPresenter<View> {
        fun onViewCreated(movie: MoviesListItem?, baseUrl: String?, posterSize: String?)
    }

    interface MovieDetailsComp {
        fun movieDetailComponent(): MovieDetailsComponent
    }
}