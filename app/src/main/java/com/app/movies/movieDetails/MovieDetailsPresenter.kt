package com.app.movies.movieDetails

import com.app.movies.core.BasePresenter
import com.app.movies.model.MoviesListItem

class MovieDetailsPresenter: BasePresenter<MovieDetailsContract.View>(), MovieDetailsContract.Presenter {

    override fun onViewCreated(movie: MoviesListItem?, baseUrl: String?, posterSize: String?) {

        bindPoster(baseUrl, posterSize, movie?.posterPath)
        bindTitle(movie?.title)
        bindReleaseDate(movie?.releaseDate)
        bindOverView(movie?.overview)
    }

    private fun bindOverView(overview: String?) {
        getView()?.bindOverview(overview)
    }

    private fun bindReleaseDate(releaseDate: String?) {
        getView()?.bindReleaseDate(releaseDate)
    }

    private fun bindTitle(title: String?) {
        getView()?.bindTitle(title)
    }

    private fun bindPoster(baseUrl: String?, posterSize: String?, posterPath: String?) {
        val url = baseUrl + posterSize + posterPath
        getView()?.displayPoster(url)
    }
}