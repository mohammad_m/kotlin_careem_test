package com.app.movies.movieDetails.di

import com.app.movies.core.di.PerFragment
import com.app.movies.movieDetails.MovieDetailsFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [MovieDetailsModule::class])
interface MovieDetailsComponent {
    fun inject(fragment: MovieDetailsFragment)
}