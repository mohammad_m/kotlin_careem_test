package com.app.movies.movieDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.movies.R
import com.app.movies.core.BaseFragment
import com.app.movies.core.di.ParentComponent
import com.app.movies.model.MoviesListItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_movie_details.view.*
import javax.inject.Inject

class MovieDetailsFragment : BaseFragment(), MovieDetailsContract.View {

    @Inject
    lateinit var presenter: MovieDetailsContract.Presenter

    companion object {
        fun newInstance(movie: MoviesListItem?, baseUrl: String?, posterSize: String?): MovieDetailsFragment {
            val args = Bundle()
            args.putParcelable(MovieDetailsContract.KEY_MOVIE_DETAILS, movie)
            args.putString(MovieDetailsContract.KEY_BASE_URL, baseUrl)
            args.putString(MovieDetailsContract.KEY_POSTER_SIZE, posterSize)
            val fragment = MovieDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInjector()
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val movie = arguments?.getParcelable<MoviesListItem>(MovieDetailsContract.KEY_MOVIE_DETAILS)
        val baseUrl = arguments?.getString(MovieDetailsContract.KEY_BASE_URL)
        val posterSize = arguments?.getString(MovieDetailsContract.KEY_POSTER_SIZE)

        presenter.onViewCreated(movie, baseUrl, posterSize)
    }

    private fun initInjector() {
        (activity as ParentComponent<MovieDetailsContract.MovieDetailsComp>).getComponent()
                .movieDetailComponent()
                .inject(this)
    }

    override fun displayPoster(url: String) {
        Glide.with(context).load(url).into(view?.posterImageView)
    }

    override fun bindTitle(title: String?) {
        view?.titleTv?.text = title
    }

    override fun bindOverview(overview: String?) {
        view?.overviewTv?.text = overview
    }

    override fun bindReleaseDate(releaseDate: String?) {
        view?.releaseDateTv?.text = releaseDate
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deAttachView()
    }
}