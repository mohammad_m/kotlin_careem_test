package com.app.movies.moviesList

import android.support.v7.widget.RecyclerView
import android.view.View
import com.app.movies.model.MoviesListItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_movies_list_item.view.*

class MoviesItemViewHolder(private val callback: MoviesItemViewHolderCallback?, itemView: View, private val posterBaseUrl: String?) : RecyclerView.ViewHolder(itemView) {

    interface MoviesItemViewHolderCallback {
        fun onMovieClicked(item: MoviesListItem?)
    }

    fun bind(item: MoviesListItem?, position: Int) {

        val posterFullUrl = posterBaseUrl + item?.posterPath

        itemView.setOnClickListener {
            callback?.onMovieClicked(item)
        }

        itemView.apply {
            nameTv.text = item?.title
            overviewTv.text = item?.overview
            releaseDateTv.text = item?.releaseDate

            Glide.with(itemView.context).load(posterFullUrl).into(posterImageView)
        }
    }

}