package com.app.movies.moviesList

import android.support.annotation.VisibleForTesting
import com.app.movies.core.BasePresenter
import com.app.movies.model.MoviesListItem
import com.app.movies.model.MoviesListMapper
import com.app.movies.model.ResponseMoviesList
import com.app.movies.usecase.MoviesListUseCase

class MoviesListPresenter(private val navigator: MoviesListContract.Navigator,
                          private val moviesListUseCase: MoviesListUseCase,
                          private val mapper: MoviesListMapper) :
        BasePresenter<MoviesListContract.View>(), MoviesListContract.Presenter {

    private var page: Int = 1

    override fun onViewCreated() {
        requestFreshMoviesList()
    }

    @VisibleForTesting
    fun requestFreshMoviesList() {
        page = 1
        loadData(page, true)
    }

    override fun loadNextPage() {
        page += 1
        loadData(page, false)
    }

    @VisibleForTesting
    fun loadData(page: Int, clearList: Boolean) {
        getView()?.showProgressBar(true)
        addDisposable(
                moviesListUseCase.execute(object : MoviesListUseCase.Callback {
                    override fun onMoviesFetched(response: ResponseMoviesList) {
                        getView()?.showProgressBar(false)
                        getView()?.bindList(mapper.mapResponseListToViewList(response), clearList)
                    }

                    override fun onMoviesFailed(throwable: Throwable) {
                        getView()?.showProgressBar(false)
                        getView()?.showRequestFailureNotification()
                    }

                }, page)
        )
    }

    override fun onMovieClicked(item: MoviesListItem?) {
        navigator.onMovieClicked(item)
    }

    override fun onFilterClicked() {
        navigator.onFilterClicked()
    }

    override fun onDataFiltered(startDate: String?, endDate: String?, filtered: Boolean?) {
        if (filtered == true) {
            getView()?.showClearFilterView(startDate, endDate)
        }
        else {
            getView()?.clearFilters()
            getView()?.showNoFilteredItemsNotification()
        }
    }

    override fun onClearFilterClicked() {
        getView()?.clearFilters()
    }

    override fun onRefreshTriggered() {
        requestFreshMoviesList()
    }
}