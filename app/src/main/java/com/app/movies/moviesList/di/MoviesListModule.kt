package com.app.movies.moviesList.di

import com.app.movies.core.di.PerFragment
import com.app.movies.model.MoviesListMapper
import com.app.movies.moviesList.MoviesListContract
import com.app.movies.moviesList.MoviesListPresenter
import com.app.movies.network.IServiceProvider
import com.app.movies.usecase.MoviesListUseCase
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class MoviesListModule(private val navigator: MoviesListContract.Navigator) {

    @Provides
    @PerFragment
    fun providePresenter(navigator: MoviesListContract.Navigator, moviesListUseCase: MoviesListUseCase,
                         mapper: MoviesListMapper): MoviesListContract.Presenter {
        return MoviesListPresenter(navigator, moviesListUseCase, mapper)
    }

    @Provides
    @PerFragment
    fun provideMoviesListUseCase(serviceProvide: IServiceProvider,
                                 @Named("ioThread") ioThread: Scheduler,
                                 @Named("mainThread") mainThread: Scheduler):
            MoviesListUseCase {

        return MoviesListUseCase(serviceProvide, ioThread, mainThread)
    }

    @Provides
    @PerFragment
    fun provideMoviesListMapper(): MoviesListMapper {
        return MoviesListMapper()
    }

    @Provides
    @PerFragment
    fun provideNavigator(): MoviesListContract.Navigator {
        return navigator
    }
}