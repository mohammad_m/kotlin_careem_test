package com.app.movies.moviesList

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.app.movies.R
import com.app.movies.core.BaseFragment
import com.app.movies.core.di.ParentComponent
import com.app.movies.model.MoviesListItem
import com.app.movies.moviesList.di.MoviesListModule
import kotlinx.android.synthetic.main.fragment_movies_list.view.*
import javax.inject.Inject

class MoviesListFragment() : BaseFragment(), MoviesListContract.View,
        MoviesItemViewHolder.MoviesItemViewHolderCallback {

    @Inject
    lateinit var presenter: MoviesListContract.Presenter

    private var adapter: MoviesListAdapter? = null

    companion object {
        fun newInstance(imageBaseUrl: String?, posterSize: String?): MoviesListFragment {
            val args = Bundle()
            args.putString(MoviesListContract.KEY_BASE_URL, imageBaseUrl)
            args.putString(MoviesListContract.KEY_POSTER_SIZE, posterSize)
            val fragment = MoviesListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInjector()
        presenter.attachView(this)
    }

    private fun initInjector() {
        (activity as ParentComponent<MoviesListContract.MoviesListComp>).getComponent()
                .moviesListComponent(MoviesListModule(activity as MoviesListContract.Navigator))
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView(view)

        view.swipeRefreshLayout.setOnRefreshListener {
            clearFilters()
            presenter.onRefreshTriggered()
        }

        view.fab.setOnClickListener {
            presenter.onFilterClicked()
        }

        presenter.onViewCreated()
    }

    private fun setupRecyclerView(view: View) {
        adapter = MoviesListAdapter(this,
                ArrayList(),
                arguments?.getString(MoviesListContract.KEY_BASE_URL, ""),
                arguments?.getString(MoviesListContract.KEY_POSTER_SIZE, ""))

        view.apply {
            recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = adapter
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (!recyclerView.canScrollVertically(1)) {
                        presenter.loadNextPage()
                    }
                }
            })
        }
    }

    override fun bindList(list: ArrayList<MoviesListItem>, clearList: Boolean) {
        if (clearList) {
            adapter?.setItems(list)
        } else {
            adapter?.appendItems(list)
        }
    }

    override fun onMovieClicked(item: MoviesListItem?) {
        presenter.onMovieClicked(item)
    }

    override fun showRequestFailureNotification() {
        Toast.makeText(context, R.string.error_fetching_movies_list, Toast.LENGTH_LONG).show()
    }

    override fun showProgressBar(show: Boolean) {
        view?.swipeRefreshLayout?.isRefreshing = show
    }

    override fun showClearFilterView(startDate: String?, endDate: String?) {
        view?.findViewById<View>(R.id.clearFilterView)?.visibility = View.VISIBLE
        view?.findViewById<TextView>(R.id.filterStartDate)?.text = startDate
        view?.findViewById<TextView>(R.id.filterEndDate)?.text = endDate
        view?.findViewById<View>(R.id.clearButton)?.setOnClickListener {
            presenter.onClearFilterClicked()
        }
    }

    override fun clearFilters() {
        view?.findViewById<View>(R.id.clearFilterView)?.visibility = View.GONE
        adapter?.clearFilters()
    }

    override fun showNoFilteredItemsNotification() {
        Toast.makeText(context, R.string.no_filtered_items, Toast.LENGTH_LONG)?.show()
    }

    fun onFilterChosen(startDate: String?, endDate: String?) {
        // apply the filter in the adapter
        val isFiltered = adapter?.filter(startDate, endDate)
        // show a view/button to clear the filter
        presenter.onDataFiltered(startDate, endDate, isFiltered)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deAttachView()
    }
}