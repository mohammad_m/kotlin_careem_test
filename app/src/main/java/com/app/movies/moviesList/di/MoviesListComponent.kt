package com.app.movies.moviesList.di

import com.app.movies.core.di.PerFragment
import com.app.movies.moviesList.MoviesListFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [MoviesListModule::class])
interface MoviesListComponent {
    fun inject(fragment: MoviesListFragment)
}