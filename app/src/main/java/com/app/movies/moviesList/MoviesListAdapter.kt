package com.app.movies.moviesList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.movies.R
import com.app.movies.model.Filter
import com.app.movies.model.MoviesListItem
import java.text.SimpleDateFormat

class MoviesListAdapter(private val callback: MoviesItemViewHolder.MoviesItemViewHolderCallback,
                        private var renderedItems: ArrayList<MoviesListItem>?,
                        imageBaseUrl: String?,
                        posterSize: String?) : RecyclerView.Adapter<MoviesItemViewHolder>() {

    private var filter: Filter? = null

    private val posterBaseUrl = imageBaseUrl + posterSize

    private val unFilteredList = ArrayList<MoviesListItem>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MoviesItemViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_movies_list_item, viewGroup, false)
        return MoviesItemViewHolder(callback, view, posterBaseUrl)
    }

    override fun getItemCount(): Int {
        return renderedItems?.size ?: 0
    }

    override fun onBindViewHolder(viewHolder: MoviesItemViewHolder, position: Int) {
        viewHolder.bind(renderedItems?.get(position), position)
    }

    /**
     * used to append the items when a new page is fetched
     */
    fun appendItems(list: ArrayList<MoviesListItem>) {
        unFilteredList.addAll(list)

        // apply the filter on the new data
        if (filter?.filterApplied == true) {
            filter(filter?.startDate, filter?.endDate)
            return
        }

        renderedItems?.addAll(list)
        notifyDataSetChanged()
    }

    /**
     * used to add the items upon fetching the first page
     */
    fun setItems(list: ArrayList<MoviesListItem>) {
        renderedItems?.clear()
        renderedItems?.addAll(list)
        unFilteredList.clear()
        unFilteredList.addAll(list)
        notifyDataSetChanged()
    }

    /**
     * return true if data is filtered, false otherwise
     */
    fun filter(startDateString: String?, endDateString: String?): Boolean {

        val formatter = SimpleDateFormat("yyyy-MM-dd")

        val startDate = formatter.parse(startDateString)
        val endDate = formatter.parse(endDateString)

        val filteredData = unFilteredList?.filter {
            formatter.parse(it.releaseDate).equals(startDate) ||
                    (formatter.parse(it.releaseDate).after(startDate) &&
                            formatter.parse(it.releaseDate).before(endDate)) ||
                    formatter.parse(it.releaseDate).equals(endDate)
        }

        if (filteredData?.size == 0) {
            return false
        }

        val isFiltered = filteredData?.size != unFilteredList?.size

        showFilteredData(ArrayList(filteredData))

        filter = Filter(startDateString, endDateString, isFiltered)

        return isFiltered
    }

    private fun showFilteredData(list: ArrayList<MoviesListItem>) {
        renderedItems?.clear()
        renderedItems?.addAll(list)
        notifyDataSetChanged()
    }

    fun clearFilters() {
        filter = null
        renderedItems?.clear()
        renderedItems?.addAll(unFilteredList)
        notifyDataSetChanged()
    }
}