package com.app.movies.moviesList

import com.app.movies.core.MvpPresenter
import com.app.movies.core.MvpView
import com.app.movies.model.MoviesListItem
import com.app.movies.moviesList.di.MoviesListComponent
import com.app.movies.moviesList.di.MoviesListModule

interface MoviesListContract {

    companion object {
        const val KEY_BASE_URL: String = "KEY_BASE_URL"
        const val KEY_POSTER_SIZE: String = "KEY_POSTER_SIZE"
    }

    interface View : MvpView {
        fun bindList(mapResponseListToViewList: ArrayList<MoviesListItem>, clearList: Boolean)
        fun showRequestFailureNotification()
        fun showProgressBar(show: Boolean)
        fun showClearFilterView(startDate: String?, endDate: String?)
        fun clearFilters()
        fun showNoFilteredItemsNotification()
    }

    interface Presenter : MvpPresenter<View> {
        fun onViewCreated()
        fun onRefreshTriggered()
        fun loadNextPage()
        fun onMovieClicked(item: MoviesListItem?)
        fun onFilterClicked()
        fun onDataFiltered(filtered: String?, endDate: String?, isFiltered: Boolean?)
        fun onClearFilterClicked()
    }

    interface Navigator {
        fun onMovieClicked(movie: MoviesListItem?)
        fun onFilterClicked()
    }

    interface MoviesListComp {
        fun moviesListComponent(module: MoviesListModule): MoviesListComponent
    }
}