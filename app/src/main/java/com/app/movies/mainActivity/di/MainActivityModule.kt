package com.app.movies.mainActivity.di

import com.app.movies.core.di.PerActivity
import com.app.movies.mainActivity.MainActivity
import com.app.movies.mainActivity.MainActivityContract
import com.app.movies.mainActivity.MainActivityPresenter
import com.app.movies.network.IServiceProvider
import com.app.movies.usecase.ConfigurationUseCase
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class MainActivityModule(val activity: MainActivity) {

    @Provides
    @PerActivity
    fun providePresenter(configurationUseCase: ConfigurationUseCase): MainActivityContract.Presenter {
        return MainActivityPresenter(configurationUseCase)
    }

    @Provides
    @PerActivity
    fun provideConfigurationUseCase(serviceProvide: IServiceProvider,
                                    @Named("ioThread") ioThread: Scheduler,
                                    @Named("mainThread") mainThread: Scheduler): ConfigurationUseCase {
        return ConfigurationUseCase(serviceProvide, ioThread, mainThread)
    }
}