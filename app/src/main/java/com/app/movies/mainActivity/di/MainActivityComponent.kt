package com.app.movies.mainActivity.di

import com.app.movies.core.di.PerActivity
import com.app.movies.filter.FilterContract
import com.app.movies.mainActivity.MainActivity
import com.app.movies.movieDetails.MovieDetailsContract
import com.app.movies.moviesList.MoviesListContract
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [MainActivityModule::class])
interface MainActivityComponent : MoviesListContract.MoviesListComp,
        MovieDetailsContract.MovieDetailsComp,
        FilterContract.FilterComp {
    fun inject(activity: MainActivity)
}