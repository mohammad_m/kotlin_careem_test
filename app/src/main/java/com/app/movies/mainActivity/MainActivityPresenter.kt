package com.app.movies.mainActivity

import com.app.movies.core.BasePresenter
import com.app.movies.model.ResponseConfiguration
import com.app.movies.usecase.ConfigurationUseCase

class MainActivityPresenter(private val configurationUseCase: ConfigurationUseCase) :
        BasePresenter<MainActivityContract.View>(), MainActivityContract.Presenter {

    override fun onActivityCreated() {
        requestConfiguration()
    }

    private fun requestConfiguration() {
        configurationUseCase.execute(object : ConfigurationUseCase.Callback {
            override fun onConfigurationFetched(response: ResponseConfiguration?) {
                getView()?.onReadyToNavigate(response?.imagesConfiguration?.baseUrl, response?.imagesConfiguration?.posterSizes?.get(0))
            }

            override fun onConfigurationFailed(throwable: Throwable) {
                getView()?.onReadyToNavigate(MainActivityContract.FALLBACK_BASE_URL, MainActivityContract.FALLBACK_POSTER_SIZE)
            }
        })
    }
}