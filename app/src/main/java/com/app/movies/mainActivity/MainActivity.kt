package com.app.movies.mainActivity

import android.os.Bundle
import com.app.movies.R
import com.app.movies.core.BaseActivity
import com.app.movies.core.MApplication
import com.app.movies.core.di.ParentComponent
import com.app.movies.filter.FilterContract
import com.app.movies.filter.FilterFragment
import com.app.movies.mainActivity.di.MainActivityComponent
import com.app.movies.mainActivity.di.MainActivityModule
import com.app.movies.model.MoviesListItem
import com.app.movies.movieDetails.MovieDetailsFragment
import com.app.movies.moviesList.MoviesListContract
import com.app.movies.moviesList.MoviesListFragment
import javax.inject.Inject

class MainActivity : BaseActivity(),
        ParentComponent<MainActivityComponent>,
        MainActivityContract.View,
        MoviesListContract.Navigator,
        FilterContract.Navigator {

    @Inject
    lateinit var presenter: MainActivityContract.Presenter

    var moviesListFragment: MoviesListFragment? = null

    var baseUrl: String? = null
    var posterSize: String? = null

    private val mainActivityComponent: MainActivityComponent by lazy {
        (application as MApplication).component.mainActivityComponent(MainActivityModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainActivityComponent.inject(this)

        presenter.attachView(this)
        presenter.onActivityCreated()
    }

    override fun onReadyToNavigate(baseUrl: String?, posterSize: String?) {
        moviesListFragment = MoviesListFragment.newInstance(baseUrl, posterSize)
        this.baseUrl = baseUrl
        this.posterSize = posterSize
        replaceFragment(moviesListFragment!!, false)
    }

    override fun getFragmentContainer(): Int {
        return R.id.fragmentContainer
    }

    override fun onApplyFilterClicked(startDate: String?, endDate: String?) {
        onBackPressed()
        moviesListFragment?.onFilterChosen(startDate, endDate)
    }

    override fun onMovieClicked(movie: MoviesListItem?) {
        addFragment(MovieDetailsFragment.newInstance(movie, baseUrl, posterSize), true)
    }

    override fun onFilterClicked() {
        addFragment(FilterFragment.newInstance(), true)
    }

    override fun getComponent(): MainActivityComponent {
        return mainActivityComponent
    }
}
