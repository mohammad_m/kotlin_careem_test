package com.app.movies.mainActivity

import com.app.movies.core.MvpPresenter
import com.app.movies.core.MvpView

interface MainActivityContract {

    companion object {
        const val FALLBACK_BASE_URL = "http://image.tmdb.org/t/p/"
        const val FALLBACK_POSTER_SIZE = "w92"
    }

    interface View: MvpView {
        fun onReadyToNavigate(baseUrl: String?, get: String?)
    }

    interface Presenter: MvpPresenter<View> {
        fun onActivityCreated()
    }
}