package com.app.movies

import com.app.movies.model.MoviesListMapper
import com.app.movies.moviesList.MoviesListContract
import com.app.movies.moviesList.MoviesListPresenter
import com.app.movies.usecase.MoviesListUseCase
import io.reactivex.disposables.Disposable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MoviesListPresenterTest {

    @Mock
    private lateinit var view: MoviesListContract.View
    @Mock
    private lateinit var navigator: MoviesListContract.Navigator
    private lateinit var presenter: MoviesListPresenter
    @Mock
    private lateinit var moviesUseCase: MoviesListUseCase
    @Mock
    private lateinit var mapper: MoviesListMapper
    @Mock
    private lateinit var moviesUseCaseCallback: MoviesListUseCase.Callback

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        moviesUseCaseCallback = Mockito.mock(MoviesListUseCase.Callback::class.java)

        presenter = Mockito.spy(MoviesListPresenter(navigator, moviesUseCase, mapper))
        presenter.attachView(view)
    }

    @Test
    fun onViewCreatedTest() {
        presenter.onViewCreated()
        Mockito.verify(presenter, Mockito.times(1)).requestFreshMoviesList()
        Mockito.verify(presenter, Mockito.times(1)).loadData(1, true)
    }

    @Test
    fun loadDataTest() {
        presenter.loadData(Mockito.anyInt(), Mockito.anyBoolean())
        Mockito.verify(view, Mockito.times(1)).showProgressBar(true)
        Mockito.verify(presenter, Mockito.times(1)).addDisposable(Mockito.any<Disposable>())
        Mockito.verify(moviesUseCase, Mockito.times(1))
                .execute(Mockito.any(MoviesListUseCase.Callback::class.java), Mockito.anyInt())
    }
}