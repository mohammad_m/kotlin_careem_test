package com.app.movies

import com.app.movies.filter.FilterContract
import com.app.movies.filter.FilterPresenter
import com.app.movies.utils.DateType
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FilterPresenterTest {

    @Mock
    private lateinit var navigator: FilterContract.Navigator
    @Mock
    private lateinit var view: FilterContract.View

    private lateinit var presenter: FilterContract.Presenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        presenter = FilterPresenter(navigator)
        presenter.attachView(view)
    }

    @Test
    fun onStartDateClickedTest() {
        presenter.onStartDateClicked()
        Mockito.verify(view, Mockito.times(1)).showDatePicker(DateType.START_DATE)
    }

    @Test
    fun onEndDateClickedTest() {
        presenter.onEndDateClicked()
        Mockito.verify(view, Mockito.times(1)).showDatePicker(DateType.END_DATE)
    }

    @Test
    fun onApplyButtonClickedValidDataTest() {
        val startDate = "02-03-2018"
        val endDate = "02-05-2018"

        presenter.onApplyButtonClicked(startDate, endDate)

        Mockito.verify(navigator, Mockito.times(1)).onApplyFilterClicked(startDate, endDate)
    }

    @Test
    fun onApplyButtonClickedEmptyDatesTest() {
        val startDate = ""
        val endDate = ""

        presenter.onApplyButtonClicked(startDate, endDate)

        Mockito.verify(view, Mockito.times(1)).showErrorMessage()
    }
}