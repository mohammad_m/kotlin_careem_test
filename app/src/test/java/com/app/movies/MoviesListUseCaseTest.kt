package com.app.movies

import com.app.movies.model.ResponseMoviesList
import com.app.movies.network.IServiceProvider
import com.app.movies.usecase.MoviesListUseCase
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class MoviesListUseCaseTest {

    private lateinit var useCase: MoviesListUseCase

    @Mock
    private lateinit var serviceProvider: IServiceProvider

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        useCase = MoviesListUseCase(serviceProvider, Schedulers.trampoline(), Schedulers.trampoline())
    }

    @Test
    fun testSuccess() {

        val page = 1

        Mockito.`when`(serviceProvider.getMoviesList(page))
                .thenReturn(Observable.just(ResponseMoviesList(ArrayList())))

        val callback: MoviesListUseCase.Callback? = Mockito.mock(MoviesListUseCase.Callback::class.java)

        useCase.execute(callback, page)
        Mockito.verify(callback, Mockito.times(1))?.onMoviesFetched(ResponseMoviesList(ArrayList()))
    }

    @Test
    fun testFailure() {

        val exception = mock<Exception>(Exception::class.java)

        val page = 1

        Mockito.`when`(serviceProvider.getMoviesList(page)).thenReturn(Observable.error(exception))

        val callback: MoviesListUseCase.Callback? = Mockito.mock(MoviesListUseCase.Callback::class.java)

        useCase.execute(callback, page)
        Mockito.verify(callback, Mockito.times(1))?.onMoviesFailed(exception)
    }
}